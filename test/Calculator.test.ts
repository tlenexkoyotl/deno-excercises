import {
  assertStrictEquals,
} from "https://deno.land/std@0.95.0/testing/asserts.ts";

import Calculator from "../src/Calculator.ts";

const additonFixtures = [
  {
    expected: 5,
    actual: {
      a: 2,
      b: 3,
    },
  },
  {
    expected: 11,
    actual: {
      a: 7,
      b: 4,
    },
  },
  {
    expected: 23,
    actual: {
      a: 17,
      b: 6,
    },
  },
];

for (const fixture of additonFixtures) {
  Deno.test({
    name: `add ${fixture.actual.a} plus ${fixture.actual.b}`,
    fn: () => {
      assertStrictEquals(
        Calculator.add(fixture.actual.a, fixture.actual.b),
        fixture.expected,
      );
    },
  });
}

Deno.test({
  name: "subtract 2 plus 3",
  fn: () => {
    assertStrictEquals(Calculator.subtract(11, 8), 3);
  },
});
